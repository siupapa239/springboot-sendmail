package kr.sskm.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@SpringBootTest
class SpringbootMailServiceTest {
	
	@Autowired
    public JavaMailSender javaMailSender;

	@Test
	void send() {
		
		String to = "받는사람이메일주소[test@test.com]";
		String subject = "제목입니다.";
		String text = "내용입니다.";
		
		SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
		
	}

}
