1. 주의사항    
Gmail사용시 보안 수준이 낮은 앱 엑세스 허용    
보안 수준이 낮은 앱 허용을 해줘야 테스트 하는 어플리케이션에서 메일 발송이 가능합니다.  
  
2. 메일발송 방법  
- application.yml 파일 username, password 발송자 정보 입력   
- SpringbootMailServiceTest 받는 사람 이메일 주소 설정 후 Junit 실행    
  
3. 참고     
https://velog.io/@npcode9194/NodeJS-nodemailer-%EB%AA%A8%EB%93%88%EC%9D%84-%EC%9D%B4%EC%9A%A9%ED%95%9C-Gmail-API-%EC%82%AC%EC%9A%A9-hhjwgcmhsh



